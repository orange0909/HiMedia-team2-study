/*
    1. EMPLOYEE 테이블 사용
       DEPT_CODE가 D1인 부서에서 근무하고 있는 사람 중
       연봉이 가장 많은 사람의 이름과 부서 이름, 연봉을 조회하자
*/
SELECT 
       MAX(A.SALARY)
  FROM EMPLOYEE A
 WHERE DEPT_CODE = 'D1';

SELECT
       A.EMP_NAME AS 이름
     , B.DEPT_TITLE AS "부서 이름"
     , A.SALARY * 12 * (1 + NVL(A.BONUS, 0)) AS 연봉
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
 WHERE A.SALARY = (SELECT MAX(A.SALARY)
                     FROM EMPLOYEE A
                    WHERE DEPT_CODE = 'D1'
                  );