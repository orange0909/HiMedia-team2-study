/*
    1. 다음 sql문이 뜻하는 의미는 ??

       SELECT *
         FROM EMPLOYEE 
        WHERE EMP_NAME LIKE 'A%'

        1) 테이블의 EMP_NAME 이 A 또는 a 로 시작하는 모든 row
   ->   2) 테이블의 EMP_NAME 이 A 로 시작하는 모든 row
        3) 테이블의 EMP_NAME 이 A 로 끝나는 모든 row
        4) 테이블의 EMP_NAME 이 A 또는 a 로 끝나는 모든 row
*/

/*
    2. 다음 테이블이 존재할 때 아래의 SQL문 실행시 출력되는 행의 건수에 대해서
       알맞게 나열한 것은 ??
       
       TAB1     COL1        COL2        KEY1
                BBB         123         B
                DDD         222         C
                EEE         233         D
                FFF         143         E
                
       TAB1     KEY2        COL1        COL2
                A           10          BC
                B           10          CD
                C           10          DE
       
       SELECT * FROM TAB1 A INNER JOIN TAB2 B ON(A.KEY1 = B.KEY2)
       SELECT * FROM TAB1 A LEFT OUTER JOIN TAB2 B ON (A.KEY1 = B.KEY2)
       SELECT * FROM TAB1 A RIGHT OUTER JOIN TAB2 B ON (A.KEY1 = B.KEY2)
       SELECT * FROM TAB1 A FULL OUTER JOIN TAB2 B On (A.KEY1 = B.KEY2)
       SELECT * FROM TAB1 A CROSS JOIN TAB2 B
       
       1) 2, 4, 3, 5, 12
       2) 2, 4, 5, 3, 12
       3) 2, 3, 4, 5, 12
       4) 2, 4, 3, 7, 12

*/