/*
    1. 전 직원의 평균 급여보다 많은 급여를 받고 있는 직원의
       사번, 이름, 직급코드, 급여를 조회하시오.
*/
SELECT
       A.EMP_ID AS 사번
     , A.EMP_NAME AS 이름
     , A.JOB_CODE AS 직급코드
     , A.SALARY AS 급여
  FROM EMPLOYEE A
 WHERE A.SALARY > (SELECT AVG(B.SALARY)
                   FROM EMPLOYEE B
                  );

/*
    2. 시스템 계정에서 KLEAGUE 계정을 생성하는 구문 작성 하시오.
*/
CREATE USER C##KLEAGUE IDENTIFIED BY KLEAGUE;

-- 권한
GRANT CONNECT, RESOURCE, CREATE VIEW TO C##KLEAGUE;