/*
    1. EMPLOYEE 테이블을 활용하여 가장 짬이 높은 5명의 사원 이름, 입사일, 월급을 조회하세요
*/
SELECT 
       A.EMP_NAME AS 이름
     , A.HIRE_DATE AS 입사일
     , A.SALARY AS 월급
  FROM EMPLOYEE A
 ORDER BY A.HIRE_DATE ASC;

SELECT
       ROWNUM
     , B.이름
     , B.입사일
     , B.월급
  FROM (SELECT A.EMP_NAME AS 이름
             , A.HIRE_DATE AS 입사일
             , A.SALARY AS 월급
          FROM EMPLOYEE A
         ORDER BY A.HIRE_DATE ASC
       ) B
 WHERE ROWNUM <= 5;
