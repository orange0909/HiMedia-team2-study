/*
    1. 부하 직원이 없는 직원(누군가의 매니저가 아닌)들의 사원번호, 이름, 입사일을 구하시오
*/

-- 전체
SELECT A.* FROM EMPLOYEE A;

-- 매니저가 있는 사원들  => ?
SELECT
       A.EMP_ID AS 사원번호
     , A.EMP_NAME AS 이름
     , A.HIRE_DATE AS 입사일
  FROM EMPLOYEE A
 WHERE A.MANAGER_ID IS NOT NULL;
-- 상사가 없는데 부하 직원도 없을 수 있음 -> 부서에 혼자있는 사람??


-- 모든 매니저들
SELECT
       DISTINCT A.MANAGER_ID
  FROM EMPLOYEE A
 WHERE A.MANAGER_ID IS NOT NULL;

-- 매니저 리스트에 포함되지 않은 사원번호의 사원들
--   => 부하 직원이 없는 사원
--   ** 상사도 없고 부하직원도 없는 사람들을 포함할 수 있음
SELECT
       A.EMP_ID AS 사원번호
     , A.EMP_NAME AS 이름
     , A.HIRE_DATE AS 입사일
  FROM EMPLOYEE A
 WHERE A.EMP_ID NOT IN (SELECT DISTINCT A.MANAGER_ID
                              FROM EMPLOYEE A
                             WHERE A.MANAGER_ID IS NOT NULL
                           )
 ORDER BY 사원번호;
/*
    2. 아시아에서 일하는 모든 직원의 급여 평균을 구하시오.
*/
SELECT * FROM LOCATION;

SELECT
       A.EMP_NAME AS 이름
     , C.LOCAL_NAME AS "지역 이름"
     , A.SALARY AS 급여
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  LEFT JOIN LOCATION C ON (B.LOCATION_ID = C.LOCAL_CODE)
 WHERE LOCAL_NAME LIKE 'ASIA%';

SELECT
       AVG(A.SALARY) AS "급여 평균"
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  LEFT JOIN LOCATION C ON (B.LOCATION_ID = C.LOCAL_CODE)
 WHERE LOCAL_NAME LIKE 'ASIA%';


/*
    3. 부서별 평균 나이를 구하시오.
*/
SELECT
       A.DEPT_CODE AS 부서코드
     , AVG(FLOOR(MONTHS_BETWEEN(SYSDATE, TO_DATE(19 || SUBSTR(A.EMP_NO, 1, 6), 'YYYYMMDD')) / 12)) AS "평균 나이"
  FROM EMPLOYEE A
 WHERE A.DEPT_CODE IS NOT NULL
 GROUP BY A.DEPT_CODE
 ORDER BY 부서코드;
