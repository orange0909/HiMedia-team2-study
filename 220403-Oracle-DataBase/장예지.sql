/*
    스크립트
*/
CREATE TABLE ANIMAL_INS(
    ANIMAL_ID VARCHAR(20),
    ANIMAL_TYPE VARCHAR(20),
    DATETIME DATE,
    INTAKE_CONDITION VARCHAR(20),
    NAME VARCHAR(20) CONSTRAINT NN_NAME NOT NULL,
    SEX_UPON_INTAKE VARCHAR(40)
);

INSERT
  INTO ANIMAL_INS
(
  ANIMAL_ID, ANIMAL_TYPE, DATETIME
, INTAKE_CONDITION, NAME, SEX_UPON_INTAKE
)
VALUES
(
  'A373219', 'Cat', TO_DATE('140729 114300', 'RRMMDD HH24MISS')
, 'Normal', 'Ella', 'Spayed Female'
);

INSERT
  INTO ANIMAL_INS
(
  ANIMAL_ID, ANIMAL_TYPE, DATETIME
, INTAKE_CONDITION, NAME, SEX_UPON_INTAKE
)
VALUES
(
  'A377750', 'Dog', TO_DATE('171025 171700', 'RRMMDD HH24MISS')
, 'Normal', 'Lucy', 'Spayed Female'
);

INSERT
  INTO ANIMAL_INS
(
  ANIMAL_ID, ANIMAL_TYPE, DATETIME
, INTAKE_CONDITION, NAME, SEX_UPON_INTAKE
)
VALUES
(
  'A354540', 'Cat', TO_DATE('141211 114800', 'RRMMDD HH24MISS')
, 'Normal', 'Tux', 'Neutered Male'
);

/*
    1. 고양이와 강아지가 각각 몇 마리인지 조회해주세요.
       순서는 고양이가 개보다 앞에 오게 조회해주세요.
*/
SELECT
       A.ANIMAL_TYPE
     , COUNT(A.ANIMAL_TYPE) AS count
  FROM ANIMAL_INS A
 GROUP BY A.ANIMAL_TYPE
 ORDER BY A.ANIMAL_TYPE;

